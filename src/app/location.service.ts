import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  location: Array<{latitude: number, longitude: number, address: string}> = [];

  constructor() { }

  setLocation(location) {
    this.location.push(location);
  }

  getLocation() {
    return this.location[this.location.length - 1];
  }

}
