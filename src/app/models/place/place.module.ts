export class Place{
  lat: number;
  long: number;
  address: string;


  constructor(lat, long, address){
      this.lat=lat;
      this.long=long;
      this.address=address;
     
  }
}