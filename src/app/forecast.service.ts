import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {
  url = 'https://api.openweathermap.org/data/2.5/forecast';
  apiKey = '156f138e8af4edfe74d12773e4b2cc34';

  constructor(private http: HttpClient) {}

  getWeatherForecast(lat, lon) {
    let params = new HttpParams()
    .set('lat', lat)
    .set('lon', lon)
    .set('units', 'metric')
    .set('appid', this.apiKey)

    return this.http.get<{list: any[], city: Record<string, string>}>(this.url, { params });
}


}
