import { Component, OnInit } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { NgZone, ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { Place } from '../models/place/place.module';
import { LocationService } from '../location.service';
import { LocalStorageService } from '../local-storage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  title = 'weather-forecast-app';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;
  location = this.locationService.getLocation();

  placeList: Place[]=(JSON.parse(localStorage["storedData"] || '[]'));
  

  @ViewChild('search')

  public searchElementRef: ElementRef;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private locationService: LocationService,
    private localStorageAs: LocalStorageService,
    
  ) { }

  ngOnInit() {
  
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          this.getAddress(this.latitude, this.longitude);
        });
      });
    });
    
  }

  addNewPlace(lat,long,address){
    this.placeList=(JSON.parse(localStorage["storedData"]));
    this.placeList.push(
      new Place(parseFloat(lat),parseFloat(long),address)
    );
    alert(address+" added to your locations");  

 this.localStorageAs.set("storedData",JSON.stringify(this.placeList));

  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
        this.getAddress(this.latitude, this.longitude);
       
      });

    }

  }
  getAddress(latitude, longitude) {

    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].address_components[results[0].address_components.length-4].long_name;
          this.locationService.setLocation({latitude, longitude, address: results[0].address_components[results[0].address_components.length-4].long_name});
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      } 

    });
  } 



}


