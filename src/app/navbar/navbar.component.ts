import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
declare var bootstrap: any;
function test(){
  var tabsNewAnim = $('#navbarSupportedContent');
  new bootstrap.Collapse(tabsNewAnim);
  var selectorNewAnim = $('#navbarSupportedContent').find('li').length;
  var activeItemNewAnim = tabsNewAnim.find('.active');
  var activeWidthNewAnimHeight = activeItemNewAnim.innerHeight();
  var activeWidthNewAnimWidth = activeItemNewAnim.innerWidth();
  var itemPosNewAnimTop = activeItemNewAnim.position();
  var itemPosNewAnimLeft = activeItemNewAnim.position();
  $(".hori-selector").css({
    "top":Math.max(itemPosNewAnimTop.top,0) + "px", 
    "left":itemPosNewAnimLeft.left + "px",
    "height": activeWidthNewAnimHeight + "px",
    "width": activeWidthNewAnimWidth + "px"
  });
  $("#navbarSupportedContent").on("click","li",function(e){
    $('#navbarSupportedContent ul li').removeClass("active");
    $(this).addClass('active');
    var activeWidthNewAnimHeight = $(this).innerHeight();
    var activeWidthNewAnimWidth = $(this).innerWidth();
    var itemPosNewAnimTop = $(this).position();
    var itemPosNewAnimLeft = $(this).position();
    $(".hori-selector").css({
      "top": Math.max(itemPosNewAnimTop.top,0) + "px", 
      "left":itemPosNewAnimLeft.left + "px",
      "height": activeWidthNewAnimHeight + "px",
      "width": activeWidthNewAnimWidth + "px"
    });
  });
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
 

  constructor() { }

  ngOnInit(): void {
    (function ($) {
      $(document).ready(function(){
        setTimeout(function(){ test(); });
      });
      $(window).on('resize', function(){
        setTimeout(function(){ test(); }, 500);
      });
      $(".navbar-toggler").click(function(){
        setTimeout(function(){ test(); });
      });
    })
    (jQuery);
    

  }
  
}

