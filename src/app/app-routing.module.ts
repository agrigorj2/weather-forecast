import { FiveDaysComponent } from './weather/five-days/five-days.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {WeatherComponent} from './weather/weather.component';
import {HomeComponent} from './home/home.component';
import { TodayComponent } from './weather/today/today.component';

const routes: Routes = [
  {path: 'weather', component: WeatherComponent},
  {path: '', component: HomeComponent},
  {path: 'current', component: TodayComponent},
  {path: 'weather/five-days', component: FiveDaysComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
