import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private latSource = new BehaviorSubject(123);
  private longSource = new BehaviorSubject(123);
  currentlat = this.latSource.asObservable();
  currentlong = this.longSource.asObservable();

  constructor() { }

  changeLat(lat: number) {
    this.latSource.next(lat);
   
  }
  changeLong(long: number) {
    this.longSource.next(long);
   
  }

}
