import { OnDestroy, SimpleChanges } from '@angular/core';
import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/data.service';
import { LocationService } from 'src/app/location.service';
import { WeatherService } from 'src/app/weather.service';

@Component({
  selector: 'app-today',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.css']
})
export class TodayComponent implements OnInit{
 lat: number;
 long: number;
  weather;
  subscription: Subscription;

  constructor(private weatherService: WeatherService, 
    private locationService: LocationService,
    private data: DataService) { }

  ngOnInit(): void {
    this.subscription =this.data.currentlat.subscribe(lat => this.lat = lat);
    this.subscription = this.data.currentlong.subscribe(long => {
      this.long = long;
      this.getWeatherByCoord();
     });
   
   
  }

  getWeatherByCoord() {
    console.log("Lat:" + this.lat+" Long: " +this.long);
      this.weatherService.getWeatherDataByCoords(this.lat, this.long).subscribe(data => {
        this.weather = data;
      })
    }
    
  

  getCity(city) {
    this.weatherService.getWeatherDataByCityName(city).subscribe(data => {
      this.weather = data;
    })
  }

}
