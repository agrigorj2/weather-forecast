import { Component, OnInit } from '@angular/core';
import { ForecastService } from 'src/app/forecast.service';
import { LocationService } from 'src/app/location.service';
import { map } from 'rxjs/operators';
import { DataService } from 'src/app/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-five-days',
  templateUrl: './five-days.component.html',
  styleUrls: ['./five-days.component.css']
})
export class FiveDaysComponent implements OnInit {
  weatherData: any = [];
  primaryDisplay = true;
  secondaryDisplay = false;

  forecastDetails: any;
  lat: number;
  long: number;
  weather;
  subscription: Subscription;
  city: Record<string,string>;
  selectedIndex: number;

  constructor(private forecastService: ForecastService, 
    private data: DataService)  { }

  ngOnInit(): void {
    this.subscription =this.data.currentlat.subscribe(lat => this.lat = lat);
    this.subscription = this.data.currentlong.subscribe(long => {
      this.long = long;
      this.getWeatherByCoord();
     });
  }

  futureForecast(data: any) {
    this.weatherData=[];
    for(let i = 0; i < data.length; i = i + 8) {
      this.weatherData.push(data[i]);
    }
    
  }

  getWeatherByCoord() {
    console.log("Lat:" + this.lat+" Long: " +this.long);
    this.forecastService.getWeatherForecast(this.lat, this.long).pipe(
      map((data) => {
        return {list: data.list, city: data.city};
      }))
      .subscribe(data => {
        console.log(data)

        this.futureForecast(data.list)
        this.city = data.city

      })
    }

    toggle(data: any, index: number) {
      this.primaryDisplay = !this.primaryDisplay;
      this.secondaryDisplay = !this.secondaryDisplay;

      this.forecastDetails = data;
      this.selectedIndex = index;
    }
    
    showDetails(data: any, i: number) {
      this.forecastDetails = data;
      this.selectedIndex = i;
    }


}
