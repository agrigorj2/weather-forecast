import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacebarComponent } from './placebar.component';

describe('PlacebarComponent', () => {
  let component: PlacebarComponent;
  let fixture: ComponentFixture<PlacebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlacebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
