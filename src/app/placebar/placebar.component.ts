import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../local-storage.service';
import { ReplaySubject, Subscription } from "rxjs";
import { Place } from '../models/place/place.module';
import { DataService } from '../data.service';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'app-placebar',
  templateUrl: './placebar.component.html',
  styleUrls: ['./placebar.component.css']
})
export class PlacebarComponent implements OnInit {
  currentlat:number;
  currentlong:number;
  subscription: Subscription;

  placeList: Place[];

  constructor(
    private localStorageAs: LocalStorageService,
    private data: DataService
  ) { }
  subEnd: ReplaySubject<any> = new ReplaySubject(1);

  ngOnInit(): void {
    this.subscription =this.data.currentlat.subscribe(lat => this.currentlat = lat);
    this.subscription = this.data.currentlong.subscribe(long => this.currentlong = long);
    
    this.localStorageAs.watch("storedData").subscribe(storedData=> {
      this.placeList=(JSON.parse(localStorage["storedData"]))
     
    
  });

}

myCurrentLocation() {
  if ('geolocation' in navigator) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.data.changeLat(position.coords.latitude); 
      this.data.changeLong(position.coords.longitude); ;
    });

  }
}

getCoordinates(lat, long){
  this.data.changeLat(lat);
  this.data.changeLong(long);
}

private removePlace(address:string): void {
  this.placeList.map((currentItem) => {
    if (currentItem.address === address) {
      let i = this.placeList.indexOf(currentItem)
      this.placeList.splice(i, 1);

    }
  });
 
  localStorage.setItem("storedData", JSON.stringify(this.placeList));
}
}

