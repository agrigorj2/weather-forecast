import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { HomeComponent } from './home/home.component';
import { WeatherComponent } from './weather/weather.component';
import { TodayComponent } from './weather/today/today.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { PlacebarComponent } from './placebar/placebar.component';
import { LocalStorageService } from './local-storage.service';
import { FiveDaysComponent } from './weather/five-days/five-days.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DataService } from './data.service';





@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WeatherComponent,
    TodayComponent,
    NavbarComponent,
    PlacebarComponent,
    FiveDaysComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBwC5CScH8qP8Hq750_s8VS_QDRd1T0HyM',
      libraries: ['places']
    }),
    HttpClientModule,
    FontAwesomeModule

  ],
  providers: [
    LocalStorageService,
    DataService,
    { provide: "WINDOW", useValue: window }
    
  ],

  bootstrap: [AppComponent]

})


export class AppModule { }
